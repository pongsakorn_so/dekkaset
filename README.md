# Note-to #

### Description ###
Note-to is the program that help users remember the work,homework,event,etc. and remind them when that work is near to the deadline.


### Feature ###
* Create a note and set its deadline and priority.
* Remind users when there are work that near the deadline.
* Sort the priority and tell the users that what work must to do now.
* Count down the date and time of the first priority work.
* Show the calender and event of each day.
* Create and show Schedule of each day.

### How do I get set up? ###
* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Team Member ###
* [Pongsakorn Somsri 5710546321](https://bitbucket.org/pongsakorn_so)
* [Parisa Supitayakul 5710546313](https://bitbucket.org/parisa_s)
* [Chinatip Vichian 5710546551](https://bitbucket.org/b5710546551)